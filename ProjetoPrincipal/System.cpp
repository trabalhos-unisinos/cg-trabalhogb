#include "System.h"
#include "Mesh.h"

System::System() {}

System::~System() {}

int System::GLFWInit()
{

	glfwInit();
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
	glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
	glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE );
	glfwWindowHint( GLFW_RESIZABLE, GL_FALSE );
	glfwWindowHint( GLFW_SAMPLES, 4 );

	window = glfwCreateWindow( WIDTH, HEIGHT, "Sabertooth", nullptr, nullptr );

	glfwGetFramebufferSize( window, &screenWidth, &screenHeight );

	if ( window == nullptr ) {
		std::cout << "Failed to create GLFW Window" << std::endl;
		glfwTerminate();

		return EXIT_FAILURE;
	}

	glfwMakeContextCurrent( window );

	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT);

	glewExperimental = GL_TRUE;

	if ( glewInit() != GLEW_OK ) {
		std::cout << "Failed no init GLEW." << std::endl;
		return EXIT_FAILURE;
	}

	glViewport( 0, 0, screenWidth, screenHeight );

	return EXIT_SUCCESS;

}

int System::OpenGLSetup()
{

	glEnable( GL_BLEND );	// Enables blending ( glBlendFunc )
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glEnable( GL_DEPTH_TEST );

	// glEnable( GL_CULL_FACE );
	// glCullFace( GL_BACK );
	// glFrontFace( GL_CW );

	return EXIT_SUCCESS;
}

int System::SystemSetup()
{

	coreShader = Shader( "Shaders/Core/core.vert", "Shaders/Core/core.frag" );
	coreShader.Use();

	return EXIT_SUCCESS;
}

void System::Run(vector<Mesh*> meshs, vector<vec3*> translatePoints)
{
	coreShader.Use();

	// glm::ortho(0.0f, (float) WIDTH, 0.0f, (float) HEIGHT, 0.1f, 100.0f);

	glm::mat4 proj = glm::perspective(glm::radians(45.0f), (float) WIDTH / (float) HEIGHT, 0.1f, 100.0f);

	coreShader.setMatrix4fv("projection", proj);

	for (Mesh* mesh : meshs) {
		for (Group* group : mesh->getGroups()) {

			Material* material = mesh->getMaterial(group->getMaterial());
			coreShader.LoadTexture(_strdup(material->getTexture().c_str()), "texture1", group->getName());
			coreShader.setVec3("materialAmbient", vec3(material->getAmbient()->x, material->getAmbient()->y, material->getAmbient()->z));
			coreShader.setVec3("materialDiffuse", vec3(material->getDiffuse()->x, material->getDiffuse()->y, material->getDiffuse()->z));
			coreShader.setVec3("materialSpecular", vec3(material->getSpecular()->x, material->getSpecular()->y, material->getSpecular()->z));
			coreShader.setFloat("materialShininess", material->getShininess());

			vector<float> vertices;
			vector<float> normais;
			vector<float> textures;

			for (Face* face : group->getFaces()) {
				for (int verticeID : face->getVertices()) {
					glm::vec3* vertice = mesh->vertice(verticeID - 1);
					vertices.push_back(vertice->x);
					vertices.push_back(vertice->y);
					vertices.push_back(vertice->z);

					group->increaseNumVertices();
				}

				for (int normalID : face->getNormais()) {
					glm::vec3* normal = mesh->normal(normalID - 1);
					normais.push_back(normal->x);
					normais.push_back(normal->y);
					normais.push_back(normal->z);
				}

				for (int textureID : face->getTextures()) {
					glm::vec2* texture = mesh->texture(textureID - 1);
					textures.push_back(texture->x);
					textures.push_back(texture->y);
				}
			}

			GLuint VBOvertices, VBOnormais, VBOtextures, VAO;
			glGenVertexArrays(1, &VAO);
			glGenBuffers(1, &VBOvertices);
			glGenBuffers(1, &VBOnormais);
			glGenBuffers(1, &VBOtextures);

			// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
			glBindVertexArray(VAO);

			// Vertices
			glBindBuffer(GL_ARRAY_BUFFER, VBOvertices);
			glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW);

			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
			glEnableVertexAttribArray(0);

			// Normais
			glBindBuffer(GL_ARRAY_BUFFER, VBOnormais);
			glBufferData(GL_ARRAY_BUFFER, normais.size() * sizeof(float), normais.data(), GL_STATIC_DRAW);

			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
			glEnableVertexAttribArray(1);

			// Textures
			glBindBuffer(GL_ARRAY_BUFFER, VBOtextures);
			glBufferData(GL_ARRAY_BUFFER, textures.size() * sizeof(float), textures.data(), GL_STATIC_DRAW);

			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
			glEnableVertexAttribArray(2);

			group->setVAO(&VAO);
			glBindVertexArray(0); // Unbind VAO
		}
	}

	float angle = 135.0f;
	int translatePoint = 0;

	float camX = 1.0f;
	float camY = 0.5f;
	float camZ = 1.0f;

	float deltaTime;
	float lastFrame = 0.0f;
	
	while ( !glfwWindowShouldClose( window ) ) {

		glfwPollEvents();

#pragma region Input Handling

		if ( glfwGetKey( window, GLFW_KEY_ESCAPE ) == GLFW_PRESS ) {
			glfwSetWindowShouldClose( window, GLFW_TRUE );
		}

		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			float currentFrame = glfwGetTime();
			deltaTime = currentFrame - lastFrame;
			
			if (deltaTime > 0.1f) {
				lastFrame = currentFrame;
				translatePoint = translatePoint - 1;
			}
		}

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			float currentFrame = glfwGetTime();
			deltaTime = currentFrame - lastFrame;

			if (deltaTime > 0.1f) {
				lastFrame = currentFrame;
				translatePoint = translatePoint + 1;
			}
		}

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			angle += 0.1f;
		}

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			angle -= 0.1f;
		}

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			camY += 0.1f;
		}

		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			camY -= 0.1f;
		}

		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			camX += 0.1f;
		}

		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			camX -= 0.1f;
		}

		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
			camX = 1.0f;
			camY = 0.5f;
			camZ = 1.0f;
		}

#pragma endregion

		glClearColor( 0.2f, 0.3f, 0.3f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		
		coreShader.Use();

		glm::mat4 view(1.0f);
		view = glm::lookAt(glm::vec3(camX, camY, camZ),
			glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 1.0f, 0.0f));

		coreShader.setMatrix4fv("view", view);

		coreShader.setVec3("lightColor", vec3(1.0f, 1.0f, 1.0f));
		coreShader.setVec3("lightPos", vec3(-100.0f, 1.0f, 100.0f));
		coreShader.setVec3("viewPos", vec3(camX, camY, camZ));

		float translateX = translatePoints[translatePoint % translatePoints.size()]->x * -1;
		float translateY = 0.0f;
		float translateZ = translatePoints[translatePoint % translatePoints.size()]->z * -1;

		glm::mat4 model(1.0f);
		model = glm::rotate(model, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::translate(model, glm::vec3(translateX, translateY, translateZ));

		coreShader.setMatrix4fv("model", model);
		for (Mesh* mesh : meshs) {
			for (Group* group : mesh->getGroups()) {
				coreShader.UseTexture(group->getName());
				glBindVertexArray(group->getVAO());
				glDrawArrays(GL_TRIANGLES, 0, group->getNumVertices());
				glBindVertexArray(0);
			}
		}

		glfwSwapBuffers(window);
	}
}

void System::Finish()
{
	coreShader.Delete();

	glfwTerminate();
}
