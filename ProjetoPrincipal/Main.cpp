#include "System.h"

#define EXIT_FAILURE -1
#define EXIT_SUCCESS 0

int main() {
	System system;

	if ( system.GLFWInit() != 0 ){
		return EXIT_FAILURE;
	}
	if ( system.OpenGLSetup() != 0 ){
		return EXIT_FAILURE;
	}
	if ( system.SystemSetup() != 0 ){
		return EXIT_FAILURE;
	}

	vector<Mesh*> meshs;
	meshs.push_back(ObjReader::read("Objects/pista.obj"));
	//meshs.push_back(ObjReader::read("Objects/mesa01.obj"));

	vector<vec3*> translatePoints = TranslatePointsReader::read("Objects/pista_coords.txt");

	system.Run(meshs, translatePoints);

	system.Finish();

	return 0;
}